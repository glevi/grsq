.. grsq documentation master file, created by
   sphinx-quickstart on Sat Oct 21 10:19:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

===============================================================================
GRSQ: Structure-Factors and X-ray Scattering from Radial Distribution Functions
===============================================================================

A package to calculate structure factors and x-ray (solution) scattering signals 
from Radial Distribution Functions (RDFs), sampled from 
Molecular Dynamics (MD) simulations.

Includes 3 different finite-size RDF-corrections, 
as well as a set of Fourier truncation window functions.

Please see https://doi.org/10.1063/5.0164365 for details 
(and cite it if you use this tool).

Example: X-ray Scattering from liquid water
===========================================
.. code-block:: python

   from grsq.grsq import rdfset_from_dir

   V = 122900.85207774633  # volume of MD box
   stoich = {'H_v': 8190, 'O_v': 4095}  # number of atoms in MD box

   # Helper function to create RDFSet, a set of RDFs 
   # that comprises all possible combinations of atom-types
   # in water: O-O, O-H, H-H. 
   rdfs = rdfset_from_dir('tests/data/xray_water/', 
                        volume=V, stoich=stoich)

   qvec = np.arange(0, 10, 0.05)  # create q vector
   ig_u = rdfs.get_iq(qvec)  # Calculate scattering
   rdfs.vdv_correct()  # apply finite-size corrections
   ig_c = rdfs.get_iq(qvec)  # Calculate scattering again

   fig, ax = plt.subplots(1, 1, figsize=(9, 5)) 
   ax.plot(qvec, ig_u, label='Raw')
   ax.plot(qvec, ig_c, label='Corrected')
   ax.set_xlim([0, 10])
   ax.set_xlabel('Q (Å$^{-1})$')
   ax.set_ylabel('I(Q)')
   ax.legend(loc='best');
   fig.tight_layout()
   
.. figure:: ../../gfx/index.png
   :alt: X-Ray Scattering of Water
   :align: center
   :figclass: align-center
   :width: 750px


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation 
   modules
   tutorials
   tutorials_other




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
