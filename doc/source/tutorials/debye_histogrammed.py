import time
import numpy as np 
import matplotlib.pyplot as plt
from ase.spacegroup import crystal
from grsq.debye import Debye

### Create heteroatomic nanoparticle for testing
a = b = 2.81  
c = 13.91  
lico2 = crystal(['Li', 'Co', 'O'],
                basis=[(0, 0, 0), 
                    (1/3, 2/3, 1/6),
                    (0, 0, 0.24)],
                spacegroup='R-3m', 
                cellpar=[a, b, c, 90, 90, 120])
lico2_chunk = lico2 * (20, 20, 10)
center = lico2_chunk.get_center_of_mass()
np_radius = 5 # 0.5 nm np
atoms = Atoms(cell=lico2_chunk.get_cell(), pbc=False)
for atom in lico2_chunk:
    if np.linalg.norm(atom.position - center) <= np_radius:
        atoms += atom

### Init Debye()
qvec = np.arange(0, 5, 0.01)
deb = Debye(qvec=qvec)
i_full = deb.debye_numba(atoms)

### Calculate the histogrammed approximation for a range of dr values
drs = [0.5, 0.2, 0.1, 0.05, 0.005, 0.001]
i_vsdr = np.zeros((len(qvec), len(drs)))
for j, dr in enumerate(drs):
    start = time.time()
    i_hist = deb.debye_histogrammed(atoms, dr=dr)
    end = time.time()
    i_vsdr[:, j] = i_hist
    
    diff = np.sum(np.abs(i_full - i_hist))
    pct_off = 100 * diff / i_full[0]
    
    print(f'dr: {dr:5.4f} Å, time: {(end - start) * 1e3:6.2f} ms, error: {pct_off:4.2f}%')


### Plot
cmap = plt.cm.viridis_r
colors = cmap(np.linspace(0, 1, len(drs)))
fig, ax = plt.subplots(1, 1, figsize=(6, 4))
ax.plot(deb.qvec[::10], i_full[::10], 'ko', mfc='none', label='Discrete Debye')
for j, dr in enumerate(drs):
    ax.plot(deb.qvec, i_vsdr[:, j], '-', color=colors[j], label=f'dr = {dr} Å')
ax.set_xlim([0.5, 5])
ax.set_ylim([0, .3e5])
ax.set_xlabel('q (Å$^{-1}$)')
ax.set_ylabel('I(q) (a.u.)')
ax.legend(loc='best', ncol=2)
fig.tight_layout()