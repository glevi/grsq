from ase.io import read
from grsq.debye import Debye
from grsq.grsq import RDF
import numpy as np
import matplotlib.pyplot as plt

# Read in an xyz file of the two particles into an ASE object
atoms = read('data/tut1/2particle.xyz')

# Define Debye object with a qvector range
deb = Debye(qvec=np.arange(0, 20, 0.01))

# Calculate scattering
i_deb = deb.debye(atoms)

### Setup the RDF object for the two atoms:

# The xyz file contains to Pt atoms:
stoich = {'Pt_u':2}

# The RDF was sampled with VMD given a square box of 10 Å
V = 10**3

# Load the sampled RDF
rdf_dat = np.genfromtxt('data/tut1/gPt_u-Pt_u.dat')

# initialse the RDF object
rdf = RDF(rdf_dat[:, 0], # r, in Å 
          rdf_dat[:, 1], # g(r) 
          'Pt', 'Pt',  # atom types: g_{Pt,Pt}(r)
          'solute', 'solute',  # atom region: this solute-only system
          qvec=deb.qvec)  # use the same q as for the debye
rdf.n1 = 2  # number of atoms
rdf.n2 = 2
rdf.volume = V  # Volume of 'simulation cell'

iq = rdf.i_term1()  # Calculate the atomic term
iq += rdf.i_term2()  # Calculate the interatomic term

# plot the RDF, so we can see what it looks like. 
fig, ax = plt.subplots(1, 1, figsize=(9, 3)) 
ax.plot(rdf_dat[:, 0 ], rdf_dat[:, 1])
ax.set_ylabel('g(r)')
ax.set_xlabel('r (Å)')
fig.tight_layout()
fig.savefig('gfx/tut1_gr.png')

# Plot the two ways of calculating I(Q) on top of each other
fig, ax = plt.subplots(1, 1, figsize=(9, 5)) 
ax.plot(deb.qvec[::10], i_deb[::10], 
        'C0o', label='Debye', mfc='None')
ax.plot(rdf.qvec,  iq, 'C2-', label='From g(r)')

ax.set_xlim([0, 20])
ax.set_xlabel('Q (Å$^{-1}$')
ax.set_ylabel('I(Q) (e.u.)')
ax.legend(loc='best')
fig.tight_layout()
fig.savefig('gfx/tut1_iq.png')