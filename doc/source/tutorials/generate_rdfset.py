import glob
import numpy as np
from grsq.grsq import RDF, RDFSet

# Data files are named 'u' for solUte, 'v' for solVent
t = {'u':'solute', 'v':'solvent'}

# Stoichometry from the MD simulation:
stoich = {'u': {'Fe':1, 'C':30, 'N':6, 'H':24},  
          'v': {'O':7329, 'H':14682}} 
# Volume from the MD simulation:
vol = 277088.4332

# List all data files, change the path to where you put them:
data_files = sorted(glob.glob('data/rdfsets/*dat'))

rdfs = RDFSet()
for df in data_files:
    data = np.genfromtxt(df)  # load the data file
    # Get r and g(r), in this case they're in these columns:
    r = data[:, 0]
    g = data[:, 1]
    # Get the element and region from the file names
    # this can be a bit cumbersome, depending on 
    # how you have stored this information
    file_name = df.split('/')[-1]
    el1 = file_name.split('g')[1].split('_')[0]
    re1 = file_name.split('_')[1].split('-')[0]
    el2 = file_name.split('-')[1].split('_')[0]
    re2 = file_name.split('_')[2].split('.')[0]
    
    # Create RDF object
    rdf = RDF(r=r, g=g, name1=el1, name2=el2,
              region1=t[re1], region2=t[re2],
              n1=stoich[re1][el1], n2=stoich[re2][el2],
              volume=vol)
    # Add g_AB to RDFSet ...
    rdfs.add_rdf(rdf)
    # .. And also g_BA:
    rdfs.add_flipped(rdf)