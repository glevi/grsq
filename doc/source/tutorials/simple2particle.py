import numpy as np
import matplotlib.pyplot as plt
from grsq.grsq import rdfset_from_dir

# Use the helper function to read in the RDF(s)
rdfs = rdfset_from_dir('data/tut1/',
                       volume=10**3,
                       stoich={'Pt_u':2})
iq = rdfs.get_iq(qvec=np.arange(0, 20, 0.01))

# Plot 
fig, ax = plt.subplots(1, 1, figsize=(9, 5)) 
# this time we just use the r vector from the first 
# - and only - rdf in the set
ax.plot(rdfs[0].qvec,  iq, 'C2-', label='From g(r)')
ax.set_xlim([0, 20])
ax.set_xlabel('Q (Å$^{-1}$')
ax.set_ylabel('I(Q) (e.u.)')
ax.legend(loc='best')
fig.tight_layout()
fig.savefig('gfx/tut1_iq_simple.png')