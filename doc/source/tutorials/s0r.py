import copy
import numpy as np
import matplotlib.pyplot as plt
from grsq.grsq import RDF
from grsq.damping import Damping

V = 100**3  # Volume of the simulation cell

data = np.genfromtxt('data/tut3/tut_gAr_v-Ar_v.dat')

# Only calculate S(q=0)
qvec = np.array([0]) 

# There is only a single RDF, so we don't need the RDFSet class here.. 
rdf_raw = RDF(r=data[:, 0], g=data[:, 1], 
              name1='Ar', name2='Ar', 
              region1='solvent', region2='solvent', 
              n1=20712, n2=20712, 
              volume=V, qvec=qvec)

# find the first nonzero g(r) r value
r_first_nonzero = rdf_raw.r[np.where(rdf_raw.g > 0)[0]][0]

# Fitted volume correction: Fit from r > 41 Å 
opt = rdf_raw.fit(r_first_nonzero, fit_start=41, fit_stop=rdf_raw.r[-1])

# Prepare to loop through all 3 corrections
corrections = ('volume', 'perera', 'vegt')
corr_vars = ({'Ri':opt.x[0]},  # use the fitted value
             {'Ri':r_first_nonzero, # as prescribed in
              'R_avg':35},   # Perera et al
             {}) # van der Vegt is parameterless

# Define R truncation values for S(0; R)
rmaxs = np.arange(0.0001, 50, 0.1)

# Define function to get S(0; R)
def get_s0(rdf, damp=None):
    s = np.zeros((len(qvec), len(rmaxs)))
    for j, r_max in enumerate(rmaxs):
        rdf.qvec = qvec
        rdf.damp = damp
        rdf.r_max = r_max
        s[:, j] = rdf.structure_factor()
    return s

# Set up the figure
fig = plt.figure(constrained_layout=True, figsize=(5, 6))
gs1 = fig.add_gridspec(nrows=4, ncols=1, left=0.05, right=0.48, hspace=0)
ax0 = fig.add_subplot(gs1[0])
ax2 = fig.add_subplot(gs1[1])
ax1 = fig.add_subplot(gs1[2:])

# Apply the corrections, and plot them
avgs = [] 
for c, corr in enumerate(corrections):
    # copy the uncorrected rdf object
    rdf_cor = copy.deepcopy(rdf_raw) 
    # apply correction
    rdf_cor.g = rdf_cor.correct(method=corr, **(corr_vars[c]))
    # get S(0; R)
    s0 = get_s0(rdf_cor, damp=None)
    ax1.plot(rmaxs, s0[0], label=corr.title())
    # Save the converged value
    avgs.append(np.mean(s0[0][rmaxs > 40]))

# plot the converged value of the Vegt correction
ax1.axhline(avgs[-1], color='k', ls='-.')
# plot the raw RDF
ax1.plot(rmaxs, get_s0(rdf_raw)[0], 'k--', label='Uncorrected')
# polish plot
ax1.legend(loc='best', fontsize=12, 
           ncol=2, columnspacing=0.5, 
           handletextpad=0.4)
ax1.set_xlabel('R (Å)')
ax1.set_ylabel('$S_c$(0; R)')
ax1.set_xlim([25, 50])
ax1.set_ylim([-.05, 0.5])

# Plot the RDF
ax0.plot(rdf_raw.r, rdf_raw.g, 'k')
ax0.set_xlabel('r (Å)')
ax0.set_ylabel('$g_{lm}$(r)')
ax0.set_yticks([0, 1, 2])
ax0.set_ylim([0, 2.95]);
ax0.set_xlim([2, 15]);
ax0.grid()

### Middle plot: Compare damped and undamped
# Using the Lorch damping window

## First plot the corrected, undamped
rdf_cor = copy.deepcopy(rdf_raw)
rdf_cor.g = rdf_cor.correct(method='volume', Ri=opt.x[0])
s0 = get_s0(rdf_cor, damp=None)
ax2.plot(rmaxs, s0[0], label='Undamped')

## Then plot the corrected, damped
# init damping object
damp = Damping('lorch', L=rdf_cor.r[-1])
# correct, damp, get_s0, plot:
rdf_cor = copy.deepcopy(rdf_raw)
rdf_cor.g = rdf_cor.correct(method='volume', Ri=opt.x[0])
s0 = get_s0(rdf_cor, damp=damp)
ax2.axhline(avgs[-1], color='k', ls='-.', zorder=30)
ax2.plot(rmaxs, s0[0], 'C0--', label='Damped')
ax2.set_xlim([40, 50])
ax2.set_ylim([0.035, 0.08])
ax2.legend(loc='upper right', fontsize=12, 
           ncol=2, columnspacing=0.35, 
           handletextpad=0.4, 
           borderaxespad=0.1)
ax2.set_ylabel('$S_c$(0; R)')
fig.savefig('gfx/tut3.png')
