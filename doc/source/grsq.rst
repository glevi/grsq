grsq package
============

grsq.grsq
---------
Contains the main classes to calculate structure factors and X-ray Scattering signals from pair RDFs, as well as correct for finite-size-sampled RDFs. 

Each RDF is treated as an object `RDF`, which contains all the neccessary attributes of an RDF sampled from MD simulations. 

All RDFs of a given system can then be collected into an `RDFSet`, which can calculate the full scattering signal, from the entire subsystem, 
the scattering from the solute-atoms only, the solvent-atoms only, or the cross term comprised of all solute-solvent pair-RDFs. 
The class can also do finite-size corrections accross all (solvent-solvent and solute-solvent) RDFs as well. 

.. automodule:: grsq.grsq
   :members:
   :undoc-members:
   :show-inheritance:

grsq.damping module
-------------------

.. automodule:: grsq.damping
   :members:
   :undoc-members:
   :show-inheritance:

grsq.debye module
-----------------

.. automodule:: grsq.debye
   :members:
   :undoc-members:
   :show-inheritance:

grsq.accel module
-----------------

.. automodule:: grsq.accel
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: grsq
   :members:
   :undoc-members:
   :show-inheritance:
